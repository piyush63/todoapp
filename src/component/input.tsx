import React, {forwardRef, useState, useImperativeHandle} from 'react';
import {TextInput, TextInputProps} from 'react-native';

const Input = function (props: TextInputProps, ref: any) {
  const [input, setInput] = useState('');

  useImperativeHandle(ref, () => input, [input]);

  return (
    <TextInput
      {...props}
      value={input}
      onChangeText={val => {
        setInput(val);
      }}
    />
  );
};

const forwardInputRef = forwardRef(Input);

export default forwardInputRef;

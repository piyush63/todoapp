/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

const activity=async () => {
    fetch('http://b686-14-141-116-242.ngrok.io/',{
      method:'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
    }).then(res=>{
      console.log("res",res)
    }).catch((err)=>{
      console.log('err',err)
    })
  };
  
  console.log("actiivity is",activity)
AppRegistry.registerHeadlessTask('ApiCall',()=>activity)
AppRegistry.registerComponent(appName, () => App);


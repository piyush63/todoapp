import {createSlice} from '@reduxjs/toolkit';

const initialState = {
  value: '',
};

const postReducer = createSlice({
  name: 'post',
  initialState,
  reducers: {
    posts(state, action) {
      console.log('submitted');
      state.value = action.payload;
    },
  },
});

export const {posts} = postReducer.actions;
export default postReducer.reducer;

import {AppRegistry} from 'react-native';

const activity = async () => {
  fetch('https://b686-14-141-116-242.ngrok.io/', {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .then(res => {
      console.log('res', res);
    })
    .catch(err => {
      console.log('err', err);
    });
};

console.log('actiivity is', activity);

AppRegistry.registerHeadlessTask('ApiCall', () => activity);

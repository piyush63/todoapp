import React from 'react';
import {ButtonProps, Button} from 'react-native';

const ButtonComponent = function (props2: ButtonProps) {
  return <Button {...props2} />;
};

export default ButtonComponent;

import React, {useEffect, useRef} from 'react';
import {StyleSheet, View, Alert} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import RNTextComponent from '../../component/text';
import RNTextInputComponent from '../../component/input';
import ButtonComponent from '../../component/buttons';
import {posts} from '../../redux/reducer';
import {AppDispatch, RootState} from '../../redux/store';

const HomeScreen = function () {
  const dispatch: AppDispatch = useDispatch();

  const inputRef = useRef('');

  const data = useSelector((state: RootState) => state.posts.value);

  const submitt = () => {
    dispatch(posts(inputRef.current));
  };

  useEffect(() => {
    console.log('data', data);
  }, [data]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <RNTextComponent
        text="please enter input below"
        onPress={() => Alert.alert('yay', 'pressed')}
      />

      <RNTextInputComponent
        // value={input}
        ref={inputRef}
        style={style.input}
      />
      <ButtonComponent onPress={submitt} title="submitt" />
    </View>
  );
};

const style = StyleSheet.create({
  input: {
    width: 200,
    height: 40,
    padding: 0,
    color: 'black',
    borderWidth: 1,
    borderColor: 'black',
  },
});

export default HomeScreen;

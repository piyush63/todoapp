//package com.myapp;
//
//public class task {
//}

package com.myapp;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import javax.annotation.Nullable;

public class Task extends HeadlessJsTaskService {

    @Override
    protected @Nullable HeadlessJsTaskConfig getTaskConfig(Intent intent) {
        Bundle extras = intent.getExtras();
        WritableMap data = extras != null ? Arguments.fromBundle(extras) : Arguments.createMap();
        if (extras != null) {
            return new HeadlessJsTaskConfig(
                "ApiCall",
                data,
                0, // timeout for the task
                false // optional: defines whether or not  the task is allowed in foreground. Default is false
            );
        }
        return null;
    }
}

